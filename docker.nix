{ pkgs ? import <nixpkgs> { }
, pkgsLinux ? import <nixpkgs> { system = "x86_64-linux"; }
}:
pkgs.dockerTools.buildImage {
  name = "haskcell-client";
  tag = "latest";
  contents =
    [
      ./dist
      pkgs.simple-http-server
      pkgs.coreutils-full
      pkgs.bash
    ];
  created = "now";
  config = {
    ExposedPorts = {
      "8000" = {};
    };
    Cmd = [ "simple-http-server" "-i"];
  };
}
